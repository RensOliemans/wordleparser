# Wordle parser

This is a python script which takes your chat history as input (exported from a
popular chat-app, for example), and outputs the average scores of all of your
group members, for a variety of wordle clones.

## Usage

    pip install wordleparse
    wordleparse chat.txt

As of now, it is only tested to work with a WhatsApp chat export.


[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)

## Supported wordle games

 - wordle
 - woordle
 - woordle6
 - worldle
 - squardle
 - crosswordle
 - primel
 - letterle
 - not_wordle
 - nerdle
 - vardle
 - diffle
 - waffle
 - heardle
 - hoordle

## Games that have to be added

 - semantle (EN)
 - semantle (NL)


## Games that might be added
 - secretwaffle
 - weekly squardle
