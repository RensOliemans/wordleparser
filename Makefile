all: lint

.PHONY: build

build:  analyse
	python setup.py sdist bdist_wheel

deploy: build
	twine upload dist/*

lint:
	black .
	pylint wordleparse/

analyse: lint
	mypy
	python -m unittest discover
